<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Cafe_Margarita_2019
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
  <img class="prefetch" src="<?php echo get_template_directory_uri(); ?>/images/viga-hover-yellow.png"  style="display: none"/>
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'cafe-margarita' ); ?></a>

	<header id="masthead" class="site-header">
    <div class="mobile-toggle">☰</div>
    <nav id="mobile-navigation">
      <?php
        wp_nav_menu( array(
          'theme_location' => 'menu-1',
          'menu_id'        => 'primary-menu',
        ) );
      ?>
    </nav>
    <nav id="site-navigation" class="main-navigation">
      <button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php esc_html_e( 'Primary Menu', 'cafe-margarita' ); ?></button>
      <?php
        wp_nav_menu( array(
          'theme_location' => 'menu-1',
          'menu_id'        => 'primary-menu',
        ) );
      ?>
    </nav><!-- #site-navigation -->
    <div class="logo">
      <img src="<?php the_field('logo', 'option'); ?>">
    </div>
	</header><!-- #masthead -->

	<div id="content" class="site-content">
    <div class="hero_image" style="background-image: url(<?php the_field('header_image', 'option'); ?>)"></div>
