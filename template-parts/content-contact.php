<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Cafe_Margarita_2019
 */

?>

<article id="contact" <?php post_class(); ?>>
  <div class="page_title">
    <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
  </div>
  <div class="page_title_shadow">
  </div>

	<div class="entry-content">
    <div>
      <?php the_content(); ?>
      <div class="catering">
        <div class="container">
          <span>We Cater Too!</span>
          <span>Book us for your next event!</span>
          <span>Fill Out Our Contact Form &raquo;</span>
        </div>
        <div class="shadow"></div>
        <img src="<?php echo get_template_directory_uri(); ?>/images/catering-chilis.png" />
      </div>
    </div>
    <div>
      <div class="contact_title">
        <div class="container">
        <h2>Contact Cafe Margarita</h2>
        </div>
        <div class="shadow"></div>
      </div>
      <?php echo do_shortcode('[contact-form-7 id="93" title="Contact Cafe Margarita"]'); ?>
    </div>
	</div><!-- .entry-content -->

	<?php if ( get_edit_post_link() ) : ?>
		<footer class="entry-footer">
			<?php
			edit_post_link(
				sprintf(
					wp_kses(
						/* translators: %s: Name of current post. Only visible to screen readers */
						__( 'Edit <span class="screen-reader-text">%s</span>', 'cafe-margarita' ),
						array(
							'span' => array(
								'class' => array(),
							),
						)
					),
					get_the_title()
				),
				'<span class="edit-link">',
				'</span>'
			);
			?>
		</footer><!-- .entry-footer -->
	<?php endif; ?>
</article><!-- #post-<?php the_ID(); ?> -->
