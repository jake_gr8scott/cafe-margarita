<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Cafe_Margarita_2019
 */

?>

<article id="<?php echo $post->post_name; ?>" <?php post_class(); ?>>
  <div class="page_title">
    <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
  </div>
  <div class="page_title_shadow">
  </div>

	<div class="entry-content">
		<?php
		the_content();

    $args = array(
      'post_parent' => $post->ID,
      'post_type'   => 'any', 
      'numberposts' => -1,
      'post_status' => 'any' ,
      'orderby' => 'menu_order',
      'order' => 'ASC',
    );
    $children = get_children( $args );

    foreach($children as $key => $child) {
      echo "<div>";
      echo "<h1>{$child->post_title}</h1>";
      echo $child->post_content;
      echo "</div>";

      if ($key == 73) {
        echo '<div class="catering">
        <div class="container">
          <span>We Cater Too!</span>
          <span>Book us for your next event!</span>
          <span><a href="/contact">Contact Us &raquo;</a></span>
        </div>
        <div class="shadow"></div>
        <img src="' . get_template_directory_uri() . '/images/catering-chilis.png" /></div>';
      }
    }

		wp_link_pages( array(
			'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'cafe-margarita' ),
			'after'  => '</div>',
		) );
		?>
	</div><!-- .entry-content -->

	<?php if ( get_edit_post_link() ) : ?>
		<footer class="entry-footer">
			<?php
			edit_post_link(
				sprintf(
					wp_kses(
						/* translators: %s: Name of current post. Only visible to screen readers */
						__( 'Edit <span class="screen-reader-text">%s</span>', 'cafe-margarita' ),
						array(
							'span' => array(
								'class' => array(),
							),
						)
					),
					get_the_title()
				),
				'<span class="edit-link">',
				'</span>'
			);
			?>
		</footer><!-- .entry-footer -->
	<?php endif; ?>
</article><!-- #post-<?php the_ID(); ?> -->
