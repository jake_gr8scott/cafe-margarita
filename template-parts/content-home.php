<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Cafe_Margarita_2019
 */

?>

<article id="home" <?php post_class(); ?>>
  <div class="page_title">
    <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
  </div>
  <div class="page_title_shadow">
  </div>

	<div class="entry-content">
		<?php
      the_content();
		?>
    <div class="extra_links">
      <?php
        $svg = [
          '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 250.48 316.97"><defs><style>.cls-1{isolation:isolate;}.cls-2{fill:#008044;opacity:0.7;mix-blend-mode:multiply;}</style></defs><title>menu-background-06</title><g class="cls-1"><g id="Home"><path class="cls-2" d="M111.54,0h27.8A111.15,111.15,0,0,1,250.48,111.15V300.44A16.53,16.53,0,0,1,233.95,317H16.53A16.53,16.53,0,0,1,0,300.44V111.54A111.54,111.54,0,0,1,111.54,0Z"/></g></g></svg>',
          '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 250.48 316.97"><defs><style>.cls-3{isolation:isolate;}.cls-4{fill:#d00080;opacity:0.7;mix-blend-mode:multiply;}</style></defs><title>history-background</title><g class="cls-3"><g id="Home"><path class="cls-4" d="M111.54,0h27.8A111.15,111.15,0,0,1,250.48,111.15V300.44A16.53,16.53,0,0,1,233.95,317H16.53A16.53,16.53,0,0,1,0,300.44V111.54A111.54,111.54,0,0,1,111.54,0Z"/></g></g></svg>',
          '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 250.48 316.97"><defs><style>.cls-5{isolation:isolate;}.cls-6{fill:#1c759d;opacity:0.7;mix-blend-mode:multiply;}</style></defs><title>events-background</title><g class="cls-5"><g id="Home"><path class="cls-6" d="M111.54,0h27.8A111.15,111.15,0,0,1,250.48,111.15V300.44A16.53,16.53,0,0,1,233.95,317H16.53A16.53,16.53,0,0,1,0,300.44V111.54A111.54,111.54,0,0,1,111.54,0Z"/></g></g></svg>',
        ];
        $i = 0;
        if( have_rows('links_below_content') ){
          while ( have_rows('links_below_content') ) { the_row();
            echo "<div>";
              echo "<a href='" . get_sub_field('link')['url'] . "'>";
                echo "<img src='" . get_sub_field('image')['sizes']['homepage-link'] . "'><br />";
                echo get_sub_field('link')['title'];
              echo "</a>";
              echo $svg[$i];
              $i++;
            echo "</div>";
          }
        }
      ?>
    </div>
	</div><!-- .entry-content -->

  <svg xmlns="http://www.w3.org/2000/svg" height="0" width="0">
    <defs>
      <clipPath id="extra_links_path">
        <path d="M111.54,0h50.34A111.15,111.15,0,0,1,273,111.15V242.59a16.53,16.53,0,0,1-16.53,16.53h-240A16.53,16.53,0,0,1,0,242.59V111.54A111.54,111.54,0,0,1,111.54,0Z"/>
      </clipPath>
    </defs>
  </svg>
</article>
