<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Cafe_Margarita_2019
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer">
		<div class="site-info">
      <div class="contact">
        <p>
          phone: (520) 457-2277
        </p>
        <p>
          address: 131 S 5th Street<br />
          Tombstone, AZ 85638
        </p>
        <p>
          <a href="#">Privacy Policy</a>
        </p>
      </div>
      <div class="links">
        <div>Cafe Margarita's Other Favorite Places</div>
        <div class="link_wrapper">
          <?php
            if( have_rows('favorite_places', 'option') ){
              while ( have_rows('favorite_places', 'option') ) { the_row();
                echo "<div>";
                  echo "<a href='" . get_sub_field('link') . "' target='_blank'>";
                    echo "<img src='" . get_sub_field('image') . "'>";
                  echo "</a>";
                echo "</div>";
              }
            }
          ?>
        </div>
      </div>
      <div class="social">
        <p>
          Hours: Closed M, Tu & W<br />
          Th & Su: 11:00 am - 7:00 pm<br />
          F & Sa: 11:00 am - 8:00 pm 
        </p>
        <div class="facebook">
          <a href="https://www.facebook.com/cafemargarita" target="_blank">
            <img src="<?php echo get_template_directory_uri(); ?>/images/facebook-icon.png">
          </a>
        </div>
      </div>
		</div><!-- .site-info -->
    <img src="<?php echo get_template_directory_uri(); ?>/images/footer-foliage.png">
	</footer><!-- #colophon -->
  <div class="sub_footer">
    <div>
      Website designed and maintained by <a href="https://gr8scottdesign.com">Great Scott Design</a>
    </div>
    <div>
      café margarita © <?php echo date('Y'); ?>
    </div>
  </div>
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
