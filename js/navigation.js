(function ($) {
  let mobile_menu_expanded = false;
  let animation_time = 250;

  $('.mobile-toggle').click(function () {
    if (mobile_menu_expanded) {
      $('#mobile-navigation').animate({
        'left': 0
      }, animation_time);
    } else {
      $('#mobile-navigation').animate({
        'left': '-300px'
      }, animation_time);
    }

    mobile_menu_expanded = !mobile_menu_expanded;
  });
})(jQuery);
