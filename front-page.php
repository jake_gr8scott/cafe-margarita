<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Cafe_Margarita_2019
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php
		while ( have_posts() ) :
			the_post();

			get_template_part( 'template-parts/content', 'home' );

		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->
	<div class="promotions">
    <div>
      <img src="<?php echo get_template_directory_uri(); ?>/images/events-chilis.png" />
      <div class="container">
        Live music every<br />
        friday & saturday<br />
        night!
      </div>
      <div class="shadow"></div>
    </div>
    <div>
      <div class="container">
        <span>We Cater Too!</span>
        <span>Book us for your next event!</span>
        <span><a href="/contact">contact us &raquo;</a></span>
      </div>
      <div class="shadow"></div>
      <img src="<?php echo get_template_directory_uri(); ?>/images/catering-chilis.png" />
    </div>
  </div>

<?php
get_footer();
